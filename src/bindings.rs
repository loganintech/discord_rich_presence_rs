/* automatically generated by rust-bindgen */

pub const _STDINT_H: u32 = 1;
pub const _FEATURES_H: u32 = 1;
pub const _DEFAULT_SOURCE: u32 = 1;
pub const __USE_ISOC11: u32 = 1;
pub const __USE_ISOC99: u32 = 1;
pub const __USE_ISOC95: u32 = 1;
pub const __USE_POSIX_IMPLICITLY: u32 = 1;
pub const _POSIX_SOURCE: u32 = 1;
pub const _POSIX_C_SOURCE: u32 = 200809;
pub const __USE_POSIX: u32 = 1;
pub const __USE_POSIX2: u32 = 1;
pub const __USE_POSIX199309: u32 = 1;
pub const __USE_POSIX199506: u32 = 1;
pub const __USE_XOPEN2K: u32 = 1;
pub const __USE_XOPEN2K8: u32 = 1;
pub const _ATFILE_SOURCE: u32 = 1;
pub const __USE_MISC: u32 = 1;
pub const __USE_ATFILE: u32 = 1;
pub const __USE_FORTIFY_LEVEL: u32 = 0;
pub const __GLIBC_USE_DEPRECATED_GETS: u32 = 0;
pub const _STDC_PREDEF_H: u32 = 1;
pub const __STDC_IEC_559__: u32 = 1;
pub const __STDC_IEC_559_COMPLEX__: u32 = 1;
pub const __STDC_ISO_10646__: u32 = 201706;
pub const __STDC_NO_THREADS__: u32 = 1;
pub const __GNU_LIBRARY__: u32 = 6;
pub const __GLIBC__: u32 = 2;
pub const __GLIBC_MINOR__: u32 = 27;
pub const _SYS_CDEFS_H: u32 = 1;
pub const __glibc_c99_flexarr_available: u32 = 1;
pub const __WORDSIZE: u32 = 64;
pub const __WORDSIZE_TIME64_COMPAT32: u32 = 1;
pub const __SYSCALL_WORDSIZE: u32 = 64;
pub const __HAVE_GENERIC_SELECTION: u32 = 1;
pub const __GLIBC_USE_LIB_EXT2: u32 = 0;
pub const __GLIBC_USE_IEC_60559_BFP_EXT: u32 = 0;
pub const __GLIBC_USE_IEC_60559_FUNCS_EXT: u32 = 0;
pub const __GLIBC_USE_IEC_60559_TYPES_EXT: u32 = 0;
pub const _BITS_TYPES_H: u32 = 1;
pub const _BITS_TYPESIZES_H: u32 = 1;
pub const __OFF_T_MATCHES_OFF64_T: u32 = 1;
pub const __INO_T_MATCHES_INO64_T: u32 = 1;
pub const __RLIM_T_MATCHES_RLIM64_T: u32 = 1;
pub const __FD_SETSIZE: u32 = 1024;
pub const _BITS_WCHAR_H: u32 = 1;
pub const _BITS_STDINT_INTN_H: u32 = 1;
pub const _BITS_STDINT_UINTN_H: u32 = 1;
pub const INT8_MIN: i32 = -128;
pub const INT16_MIN: i32 = -32768;
pub const INT32_MIN: i32 = -2147483648;
pub const INT8_MAX: u32 = 127;
pub const INT16_MAX: u32 = 32767;
pub const INT32_MAX: u32 = 2147483647;
pub const UINT8_MAX: u32 = 255;
pub const UINT16_MAX: u32 = 65535;
pub const UINT32_MAX: u32 = 4294967295;
pub const INT_LEAST8_MIN: i32 = -128;
pub const INT_LEAST16_MIN: i32 = -32768;
pub const INT_LEAST32_MIN: i32 = -2147483648;
pub const INT_LEAST8_MAX: u32 = 127;
pub const INT_LEAST16_MAX: u32 = 32767;
pub const INT_LEAST32_MAX: u32 = 2147483647;
pub const UINT_LEAST8_MAX: u32 = 255;
pub const UINT_LEAST16_MAX: u32 = 65535;
pub const UINT_LEAST32_MAX: u32 = 4294967295;
pub const INT_FAST8_MIN: i32 = -128;
pub const INT_FAST16_MIN: i64 = -9223372036854775808;
pub const INT_FAST32_MIN: i64 = -9223372036854775808;
pub const INT_FAST8_MAX: u32 = 127;
pub const INT_FAST16_MAX: u64 = 9223372036854775807;
pub const INT_FAST32_MAX: u64 = 9223372036854775807;
pub const UINT_FAST8_MAX: u32 = 255;
pub const UINT_FAST16_MAX: i32 = -1;
pub const UINT_FAST32_MAX: i32 = -1;
pub const INTPTR_MIN: i64 = -9223372036854775808;
pub const INTPTR_MAX: u64 = 9223372036854775807;
pub const UINTPTR_MAX: i32 = -1;
pub const PTRDIFF_MIN: i64 = -9223372036854775808;
pub const PTRDIFF_MAX: u64 = 9223372036854775807;
pub const SIG_ATOMIC_MIN: i32 = -2147483648;
pub const SIG_ATOMIC_MAX: u32 = 2147483647;
pub const SIZE_MAX: i32 = -1;
pub const WINT_MIN: u32 = 0;
pub const WINT_MAX: u32 = 4294967295;
pub const DISCORD_REPLY_NO: u32 = 0;
pub const DISCORD_REPLY_YES: u32 = 1;
pub const DISCORD_REPLY_IGNORE: u32 = 2;
pub type __u_char = ::std::os::raw::c_uchar;
pub type __u_short = ::std::os::raw::c_ushort;
pub type __u_int = ::std::os::raw::c_uint;
pub type __u_long = ::std::os::raw::c_ulong;
pub type __int8_t = ::std::os::raw::c_schar;
pub type __uint8_t = ::std::os::raw::c_uchar;
pub type __int16_t = ::std::os::raw::c_short;
pub type __uint16_t = ::std::os::raw::c_ushort;
pub type __int32_t = ::std::os::raw::c_int;
pub type __uint32_t = ::std::os::raw::c_uint;
pub type __int64_t = ::std::os::raw::c_long;
pub type __uint64_t = ::std::os::raw::c_ulong;
pub type __quad_t = ::std::os::raw::c_long;
pub type __u_quad_t = ::std::os::raw::c_ulong;
pub type __intmax_t = ::std::os::raw::c_long;
pub type __uintmax_t = ::std::os::raw::c_ulong;
pub type __dev_t = ::std::os::raw::c_ulong;
pub type __uid_t = ::std::os::raw::c_uint;
pub type __gid_t = ::std::os::raw::c_uint;
pub type __ino_t = ::std::os::raw::c_ulong;
pub type __ino64_t = ::std::os::raw::c_ulong;
pub type __mode_t = ::std::os::raw::c_uint;
pub type __nlink_t = ::std::os::raw::c_ulong;
pub type __off_t = ::std::os::raw::c_long;
pub type __off64_t = ::std::os::raw::c_long;
pub type __pid_t = ::std::os::raw::c_int;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct __fsid_t {
    pub __val: [::std::os::raw::c_int; 2usize],
}
#[test]
fn bindgen_test_layout___fsid_t() {
    assert_eq!(
        ::std::mem::size_of::<__fsid_t>(),
        8usize,
        concat!("Size of: ", stringify!(__fsid_t))
    );
    assert_eq!(
        ::std::mem::align_of::<__fsid_t>(),
        4usize,
        concat!("Alignment of ", stringify!(__fsid_t))
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<__fsid_t>())).__val as *const _ as usize },
        0usize,
        concat!(
            "Offset of field: ",
            stringify!(__fsid_t),
            "::",
            stringify!(__val)
        )
    );
}
pub type __clock_t = ::std::os::raw::c_long;
pub type __rlim_t = ::std::os::raw::c_ulong;
pub type __rlim64_t = ::std::os::raw::c_ulong;
pub type __id_t = ::std::os::raw::c_uint;
pub type __time_t = ::std::os::raw::c_long;
pub type __useconds_t = ::std::os::raw::c_uint;
pub type __suseconds_t = ::std::os::raw::c_long;
pub type __daddr_t = ::std::os::raw::c_int;
pub type __key_t = ::std::os::raw::c_int;
pub type __clockid_t = ::std::os::raw::c_int;
pub type __timer_t = *mut ::std::os::raw::c_void;
pub type __blksize_t = ::std::os::raw::c_long;
pub type __blkcnt_t = ::std::os::raw::c_long;
pub type __blkcnt64_t = ::std::os::raw::c_long;
pub type __fsblkcnt_t = ::std::os::raw::c_ulong;
pub type __fsblkcnt64_t = ::std::os::raw::c_ulong;
pub type __fsfilcnt_t = ::std::os::raw::c_ulong;
pub type __fsfilcnt64_t = ::std::os::raw::c_ulong;
pub type __fsword_t = ::std::os::raw::c_long;
pub type __ssize_t = ::std::os::raw::c_long;
pub type __syscall_slong_t = ::std::os::raw::c_long;
pub type __syscall_ulong_t = ::std::os::raw::c_ulong;
pub type __loff_t = __off64_t;
pub type __caddr_t = *mut ::std::os::raw::c_char;
pub type __intptr_t = ::std::os::raw::c_long;
pub type __socklen_t = ::std::os::raw::c_uint;
pub type __sig_atomic_t = ::std::os::raw::c_int;
pub type int_least8_t = ::std::os::raw::c_schar;
pub type int_least16_t = ::std::os::raw::c_short;
pub type int_least32_t = ::std::os::raw::c_int;
pub type int_least64_t = ::std::os::raw::c_long;
pub type uint_least8_t = ::std::os::raw::c_uchar;
pub type uint_least16_t = ::std::os::raw::c_ushort;
pub type uint_least32_t = ::std::os::raw::c_uint;
pub type uint_least64_t = ::std::os::raw::c_ulong;
pub type int_fast8_t = ::std::os::raw::c_schar;
pub type int_fast16_t = ::std::os::raw::c_long;
pub type int_fast32_t = ::std::os::raw::c_long;
pub type int_fast64_t = ::std::os::raw::c_long;
pub type uint_fast8_t = ::std::os::raw::c_uchar;
pub type uint_fast16_t = ::std::os::raw::c_ulong;
pub type uint_fast32_t = ::std::os::raw::c_ulong;
pub type uint_fast64_t = ::std::os::raw::c_ulong;
pub type intmax_t = __intmax_t;
pub type uintmax_t = __uintmax_t;
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct DiscordRichPresence {
    pub state: *const ::std::os::raw::c_char,
    pub details: *const ::std::os::raw::c_char,
    pub startTimestamp: i64,
    pub endTimestamp: i64,
    pub largeImageKey: *const ::std::os::raw::c_char,
    pub largeImageText: *const ::std::os::raw::c_char,
    pub smallImageKey: *const ::std::os::raw::c_char,
    pub smallImageText: *const ::std::os::raw::c_char,
    pub partyId: *const ::std::os::raw::c_char,
    pub partySize: ::std::os::raw::c_int,
    pub partyMax: ::std::os::raw::c_int,
    pub matchSecret: *const ::std::os::raw::c_char,
    pub joinSecret: *const ::std::os::raw::c_char,
    pub spectateSecret: *const ::std::os::raw::c_char,
    pub instance: i8,
}
#[test]
fn bindgen_test_layout_DiscordRichPresence() {
    assert_eq!(
        ::std::mem::size_of::<DiscordRichPresence>(),
        112usize,
        concat!("Size of: ", stringify!(DiscordRichPresence))
    );
    assert_eq!(
        ::std::mem::align_of::<DiscordRichPresence>(),
        8usize,
        concat!("Alignment of ", stringify!(DiscordRichPresence))
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordRichPresence>())).state as *const _ as usize },
        0usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(state)
        )
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordRichPresence>())).details as *const _ as usize },
        8usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(details)
        )
    );
    assert_eq!(
        unsafe {
            &(*(::std::ptr::null::<DiscordRichPresence>())).startTimestamp as *const _ as usize
        },
        16usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(startTimestamp)
        )
    );
    assert_eq!(
        unsafe {
            &(*(::std::ptr::null::<DiscordRichPresence>())).endTimestamp as *const _ as usize
        },
        24usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(endTimestamp)
        )
    );
    assert_eq!(
        unsafe {
            &(*(::std::ptr::null::<DiscordRichPresence>())).largeImageKey as *const _ as usize
        },
        32usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(largeImageKey)
        )
    );
    assert_eq!(
        unsafe {
            &(*(::std::ptr::null::<DiscordRichPresence>())).largeImageText as *const _ as usize
        },
        40usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(largeImageText)
        )
    );
    assert_eq!(
        unsafe {
            &(*(::std::ptr::null::<DiscordRichPresence>())).smallImageKey as *const _ as usize
        },
        48usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(smallImageKey)
        )
    );
    assert_eq!(
        unsafe {
            &(*(::std::ptr::null::<DiscordRichPresence>())).smallImageText as *const _ as usize
        },
        56usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(smallImageText)
        )
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordRichPresence>())).partyId as *const _ as usize },
        64usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(partyId)
        )
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordRichPresence>())).partySize as *const _ as usize },
        72usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(partySize)
        )
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordRichPresence>())).partyMax as *const _ as usize },
        76usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(partyMax)
        )
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordRichPresence>())).matchSecret as *const _ as usize },
        80usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(matchSecret)
        )
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordRichPresence>())).joinSecret as *const _ as usize },
        88usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(joinSecret)
        )
    );
    assert_eq!(
        unsafe {
            &(*(::std::ptr::null::<DiscordRichPresence>())).spectateSecret as *const _ as usize
        },
        96usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(spectateSecret)
        )
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordRichPresence>())).instance as *const _ as usize },
        104usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordRichPresence),
            "::",
            stringify!(instance)
        )
    );
}
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct DiscordUser {
    pub userId: *const ::std::os::raw::c_char,
    pub username: *const ::std::os::raw::c_char,
    pub discriminator: *const ::std::os::raw::c_char,
    pub avatar: *const ::std::os::raw::c_char,
}
#[test]
fn bindgen_test_layout_DiscordUser() {
    assert_eq!(
        ::std::mem::size_of::<DiscordUser>(),
        32usize,
        concat!("Size of: ", stringify!(DiscordUser))
    );
    assert_eq!(
        ::std::mem::align_of::<DiscordUser>(),
        8usize,
        concat!("Alignment of ", stringify!(DiscordUser))
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordUser>())).userId as *const _ as usize },
        0usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordUser),
            "::",
            stringify!(userId)
        )
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordUser>())).username as *const _ as usize },
        8usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordUser),
            "::",
            stringify!(username)
        )
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordUser>())).discriminator as *const _ as usize },
        16usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordUser),
            "::",
            stringify!(discriminator)
        )
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordUser>())).avatar as *const _ as usize },
        24usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordUser),
            "::",
            stringify!(avatar)
        )
    );
}
#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct DiscordEventHandlers {
    pub ready: ::std::option::Option<unsafe extern "C" fn(request: *const DiscordUser)>,
    pub disconnected: ::std::option::Option<
        unsafe extern "C" fn(
            errorCode: ::std::os::raw::c_int,
            message: *const ::std::os::raw::c_char,
        ),
    >,
    pub errored: ::std::option::Option<
        unsafe extern "C" fn(
            errorCode: ::std::os::raw::c_int,
            message: *const ::std::os::raw::c_char,
        ),
    >,
    pub joinGame:
        ::std::option::Option<unsafe extern "C" fn(joinSecret: *const ::std::os::raw::c_char)>,
    pub spectateGame:
        ::std::option::Option<unsafe extern "C" fn(spectateSecret: *const ::std::os::raw::c_char)>,
    pub joinRequest: ::std::option::Option<unsafe extern "C" fn(request: *const DiscordUser)>,
}
#[test]
fn bindgen_test_layout_DiscordEventHandlers() {
    assert_eq!(
        ::std::mem::size_of::<DiscordEventHandlers>(),
        48usize,
        concat!("Size of: ", stringify!(DiscordEventHandlers))
    );
    assert_eq!(
        ::std::mem::align_of::<DiscordEventHandlers>(),
        8usize,
        concat!("Alignment of ", stringify!(DiscordEventHandlers))
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordEventHandlers>())).ready as *const _ as usize },
        0usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordEventHandlers),
            "::",
            stringify!(ready)
        )
    );
    assert_eq!(
        unsafe {
            &(*(::std::ptr::null::<DiscordEventHandlers>())).disconnected as *const _ as usize
        },
        8usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordEventHandlers),
            "::",
            stringify!(disconnected)
        )
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordEventHandlers>())).errored as *const _ as usize },
        16usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordEventHandlers),
            "::",
            stringify!(errored)
        )
    );
    assert_eq!(
        unsafe { &(*(::std::ptr::null::<DiscordEventHandlers>())).joinGame as *const _ as usize },
        24usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordEventHandlers),
            "::",
            stringify!(joinGame)
        )
    );
    assert_eq!(
        unsafe {
            &(*(::std::ptr::null::<DiscordEventHandlers>())).spectateGame as *const _ as usize
        },
        32usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordEventHandlers),
            "::",
            stringify!(spectateGame)
        )
    );
    assert_eq!(
        unsafe {
            &(*(::std::ptr::null::<DiscordEventHandlers>())).joinRequest as *const _ as usize
        },
        40usize,
        concat!(
            "Offset of field: ",
            stringify!(DiscordEventHandlers),
            "::",
            stringify!(joinRequest)
        )
    );
}
extern "C" {
    pub fn Discord_Initialize(
        applicationId: *const ::std::os::raw::c_char,
        handlers: *mut DiscordEventHandlers,
        autoRegister: ::std::os::raw::c_int,
        optionalSteamId: *const ::std::os::raw::c_char,
    );
}
extern "C" {
    pub fn Discord_Shutdown();
}
extern "C" {
    pub fn Discord_RunCallbacks();
}
extern "C" {
    pub fn Discord_UpdatePresence(presence: *const DiscordRichPresence);
}
extern "C" {
    pub fn Discord_ClearPresence();
}
extern "C" {
    pub fn Discord_Respond(userid: *const ::std::os::raw::c_char, reply: ::std::os::raw::c_int);
}
extern "C" {
    pub fn Discord_UpdateHandlers(handlers: *mut DiscordEventHandlers);
}
