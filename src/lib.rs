#![allow(dead_code)]
#![allow(non_snake_case)]
#![allow(non_upper_case_globals)]


mod bindings;
use crate::bindings::*;
use std::result::Result;

use std::ffi::CString;

pub struct Presence {
    handlers: DiscordEventHandlers,
}

pub struct PresenceBuilder {
    client_id: Option<CString>,
    handlers: Option<DiscordEventHandlers>,
    auto_register: Option<i32>,
    steam_id: Option<CString>,
}

enum RPCError {
    ConnectionFailed,
    MissingField,
}

impl PresenceBuilder {
    fn with_id(mut self, client_id: String) -> PresenceBuilder {
        self.client_id = Some(CString::new(client_id).unwrap());
        self
    }

    fn with_steam_id(mut self, steam_id: String) -> PresenceBuilder {
        self.steam_id = Some(CString::new(steam_id).unwrap());
        self
    }

    fn with_handlers(mut self, handlers: DiscordEventHandlers) -> PresenceBuilder {
        self.handlers = Some(handlers);
        self
    }

    fn auto_register(mut self, reg: i32) -> PresenceBuilder {
        self.auto_register = Some(reg);
        self
    }

    fn connect(self) -> Result<Presence, RPCError> {
        if self.client_id.is_none() || self.handlers.is_none() || self.auto_register.is_none() {
            return Err(RPCError::MissingField);
        }

        let mut presence = Presence {
            handlers: self.handlers.unwrap(),
        };

        unsafe {
            Discord_Initialize(
                self.client_id.unwrap().as_ptr(),
                &mut presence.handlers,
                self.auto_register.unwrap(),
                self.steam_id.unwrap_or(CString::new("").unwrap()).as_ptr(),
            );
        }

        Ok(presence)
    }
}

impl Presence {
    fn new() -> PresenceBuilder {
        PresenceBuilder {
            client_id: None,
            handlers: None,
            auto_register: None,
            steam_id: None,
        }
    }
}

impl Drop for Presence {
    fn drop(&mut self) {
        unsafe {
            Discord_Shutdown();
        }
    }
}
