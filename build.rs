use std::path::PathBuf;
use cmake::Config;

fn main() {

    let mut dst: PathBuf = Config::new("discord-rpc")
                // .generator("Visual Studio 15 2017 Win64")
                .profile("Release")
                .build();

    dst.push("lib");
    println!(r"cargo:rustc-link-search=native={}", dst.display());
    println!(r"cargo:rustc-link-lib=static=discord-rpc");

    let bindings = bindgen::Builder::default().header("./discord-rpc/include/discord_rpc.h").generate().expect("Can't generate bindings.");
    let out_dir = PathBuf::from("./src");
    bindings.write_to_file(out_dir.join("bindings.rs"))
            .expect("Couldn't write bindings.");
}
